<?php
session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gaming actu</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Custom icon font-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- Google fonts - Open Sans-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
    <!-- Fancybox-->
    <link rel="stylesheet" href="vendor/@fancyapps/fancybox/jquery.fancybox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <header class="header">
      <!-- Main Navbar-->
      <nav class="navbar navbar-expand-lg fixed-top">
        <div class="search-area">
          <div class="search-area-inner d-flex align-items-center justify-content-center ">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="row d-flex justify-content-center">
              <div class="col-md-8">
                <form action="#">
                  <div class="form-group">
                    <input type="search" name="search" id="search" placeholder="Qu'est ce que vous voulez rechercher ?" >
                    <button type="submit" class="submit"><i class="icon-search-1"></i></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          
          <div class="navbar-header d-flex align-items-center justify-content-between">
            <a href="index.php" class="navbar-brand"><img src="Image/logo.png" style="height:100px;"></a>
            
            <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarcollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span></span><span></span><span></span></button>
          </div>
          
          <div id="navbarcollapse" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item"><a href="index.php" class="nav-link active ">Accueil</a>
              </li>
              <li class="nav-item"><a href="actualite.php" class="nav-link ">Actualité</a>
              </li>
              <li class="nav-item"><a href="comparateur.php" class="nav-link ">Comparateur</a>
              </li>
			  <li class="nav-item"><a href="Jeux.php" class="nav-link ">Jeux</a>
              </li>
			    			  <li class="nav-item"><a href="Snake.php" class="nav-link ">Snake</a>
              </li>
			  <?php 
 if(isset($_SESSION["autorisationUtil"]))
			  {
				   ?>
			<li class="nav-item"><a href="accueilUtil.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexion.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"])){
				  ?>
			<li class="nav-item"><a href="admin.php" class="nav-link ">Mon Compte</a>
              </li>
			  <li class="nav-item"><a href="deconnexionAdmin.php" class="nav-link ">Se déconnecter</a>
              </li>
			  <?php
			  }elseif(!isset($_SESSION["autorisationAdmin"])&& !isset($_SESSION["autorisationUtil"]) ){
				  
				  
				  
			  
				   ?>
			  <li class="nav-item"><a href="login.php" class="nav-link ">Membre</a>
              </li>
			  <li class="nav-item"><a href="loginAdmin.php" class="nav-link ">Admin</a>
              </li>
			 <?php
			  }
			  ?>
            </ul>
           <div class="navbar-text">
			
			<form class="form-inline my-2 my-lg-0" action="recherche.php" method="POST">
				<input class="form-control mr-sm-2" type="search" name="produit" placeholder="Produit" aria-label="Produit">
				<button class="btn btn-outline-primary my-2 my-sm-0" type="submit"> <i class="icon-search-1"></i> </button>
			</form>
            </div>
            
            
          </div>
        </div>
      </nav>
    </header>
    
    <section style="background: url(Image/game.jpg); background-size: cover; background-position: center center;border-style:inset;margin-top:5%;" class="hero">
      <div class="container">
		</div>	  
    </section>
	<br>
	<hr class="my-4" style="background-color:black;">
	<h1 style="text-align: center;font-family: 'Anton', sans-serif;"><i class="fas  fa-2x fa-broadcast-tower"></i> Actualités du 03/05/2019 :</h1>
	
		<?php
//connexion à la BDD via la fonction de connect de la page PdoBonbon
require "ConnexionBDD.php";
$bdd=connect();
//requête
$sql="select * from jeux where nomJeu LIKE (lower('Fifa 19')) ";
$resultat = $bdd -> query($sql);
$produit=$resultat->fetch(PDO::FETCH_OBJ)
	
	?>
    <div class="container" style="display: inline-block;margin-left:25%;" >
	<div class="row">
	<div class="col md-4">
    <section class="actualité">
		<div class="container">
			<div class="row">
				
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;">
					<h1 class="display-4"style="color:white;">Actualité</h1>
					<p class="lead"style="color:white;"><?php echo $produit-> nomJeu;?></p>
					<img src="./<?php echo $produit->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4"></hr>
					<p style="color:white;">Une nouvelle Mise à jour de fifa 19 vient de sortir !</p>
					<a class="btn btn-primary btn-lg" href="ActuFifa.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
			</div>
			</div>
		</div>
	</section>
	</div>
	<?php
	$sql2="select * from jeux where nomJeu LIKE (lower('Super Smash Bros Ultimate')) ";
	$resultat2 = $bdd -> query($sql2);
	$produit2=$resultat2->fetch(PDO::FETCH_OBJ)
	?>
	
	<div class="row">
	<div class="col md-4">
    <section class="actualité">
		<div class="container" >
			<div class="row">
		
			<div class="row">
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;">
					<h1 class="display-4" style="color:white;">Actualité</h1>
					<p class="lead" style="color:white;"><?php echo $produit2-> nomJeu;?></p>
					<img src="./<?php echo $produit2->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4"></hr>
					<p style="color:white;">Une nouvelle Mise à jour de Smash Bros Ultimate vient de sortir !</p>
					<a class="btn btn-primary btn-lg" href="ActuSmash.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
				
			</div>
			
		</div>
	</section>
	</div>
	</div>
	</div>
	</div>
	<hr class="my-5" style="background-color:black;">
	
	<h1 style="text-align: center;center;font-family: 'Anton', sans-serif;"><i class="fas  fa-2x fa-broadcast-tower"></i>Actualités du 02/05/2019 :</h1>
	<?php
	$sql2="select * from jeux where nomJeu LIKE (lower('Dragon Ball FighterZ')) ";
	$resultat2 = $bdd -> query($sql2);
	$produit2=$resultat2->fetch(PDO::FETCH_OBJ)
	?>
	<div class="container" style="display: inline-block;margin-left:25%;" >
	<section class="actualité">
		<div class="container">
			<div class="row">
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;">
					<h1 class="display-4" style="color:white;">Actualité</h1>
					<p class="lead" style="color:white;"><?php echo $produit2-> nomJeu;?></p>
					<img src="./<?php echo $produit2->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4"></hr>
					<p style="color:white;">Une nouvelle Mise à jour de Dragon Ball FighterZ vient d'etre déclarer</p>
					<a class="btn btn-primary btn-lg" href="ActuDBZ.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
				
			</div>
			
		</div>
	</section>
	
	<?php
	$sql2="select * from jeux where nomJeu LIKE (lower('The Division 2')) ";
	$resultat2 = $bdd -> query($sql2);
	$produit2=$resultat2->fetch(PDO::FETCH_OBJ)
	?>
	
	<section class="actualité" >
		<div class="container">
			<div class="row">
				<div class="jumbotron" style="background: url(img/texture.jpg); background-size: cover;">
					<h1 class="display-4" style="color:white;">Actualité</h1>
					<p class="lead" style="color:white;"><?php echo $produit2-> nomJeu;?></p>
					<img src="./<?php echo $produit2->imageJeu?>"style="height:500px;width:800px;">
					<hr class="my-4"></hr>
					<p style="color:white;">Les actualités concernant The Division 2</p>
					<a class="btn btn-primary btn-lg" href="ActuDivision2.php" role="button"><i class="fab  fa-readme"> Lire l'article</i></a>
				</div>
				
			</div>
			
		</div>
	</section>
	</div>
	
	<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item disabled">
      <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
    </li>
    <li class="page-item"><a class="page-link" href="actualite.php">1</a></li>
    <li class="page-item"><a class="page-link" href="actu-2.php">2</a></li>
    <li class="page-item"><a class="page-link" href="actu-3.php">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#">Next</a>
    </li>
  </ul>
</nav>
<br>

	<footer class="main-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="logo">
              
            </div>
            <div class="contact-details">
              <h3>Contact Staff</h3>
			  <div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/luffy.jpg" alt="..." class="img-fluid"></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">fatih-altintas.pro@outlook.fr</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/goku.jpg" alt="..." class="img-fluid" style=""></div>
                  <div class="title"><strong>Email:</strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">nassim.bougtib@hotmail.com</a></span></div>
                </div>
				<br>
				<div class="post d-flex align-items-center">
                  <div class="image"><img src="Image/jojo.jpg" alt="..." class="img-fluid" style="height:150px;"></div>
                  <div class="title"><strong>Email:<br></strong><span class="date last-meta"><a href="mailto:info@company.com"style="font-size:11px;">firask_92@hotmail.fr</a></span></div>
                </div>
            </div>
          </div>
          
          
        </div>
      </div>
	  <br>
    <br>
    
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2019. All rights reserved.</p>
            </div>
            
          </div>
        </div>
      </div>
    </footer>
</body>
</html>